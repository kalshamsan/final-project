﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CountDown : MonoBehaviour
{

    float timeRemaining = 240;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timeRemaining -= Time.deltaTime;
    }

    void OnGUI()
    {
        if (timeRemaining > 0)
        {
            GUI.Label(new Rect(70, 60, 200, 100), "Time: " + (int)timeRemaining);
        }
        else
        {
            GUI.Label(new Rect(500, 50, 100, 100), "Time's up");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
        }
        }
}
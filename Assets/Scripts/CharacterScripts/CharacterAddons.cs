﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]

public class CharacterAddons : MonoBehaviour
{
    Animator animator;

    //count variables
    private int count;
    public Text winText;
    public Text loseText;
    public Text countText;
    //public AudioClip SoundToPlay;
  // public float Volume;
   // new AudioSource audio;
    public Image currentHealthBar;
    public float totalHealth = 100f;
    public float currHealth = 0f;
    float timeRemaining = 120;


    // Use this for initialization
    private void Start()
    {
        animator = GetComponent<Animator>();
       //audio = GetComponent<AudioSource>();
        count = 0;
        SetCountText();
        currHealth = totalHealth;
        SetHealthBar();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetBool("DeathTrigger", true);
           // audio.PlayOneShot(SoundToPlay, Volume);
        }
        if (Input.GetKey(KeyCode.X))
        {
            animator.SetBool("DeathTrigger", false);
        }

        timeRemaining = Time.deltaTime;

    }

    void onGUI()
    {
        if (timeRemaining > 0)
        {
            GUI.Label(new Rect(100, 100, 200, 100), "Time Remaining : " + (int)timeRemaining);
        }
        else
        {
            GUI.Label(new Rect(100, 100, 100, 100), "Time's up");
        }

    }
   

    public void SetHealthBar()
    {
        float my_health = currHealth / totalHealth;
        currentHealthBar.transform.localScale = new Vector3(Mathf.Clamp(my_health, 0f, 1f), currentHealthBar.transform.localScale.y, currentHealthBar.transform.localScale.z);
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        else { TakeDamage(5f); }

    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count == 105)
        {
            winText.text = "You Win!";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
        }
    }

    public void TakeDamage(float damage)
    {
        currHealth -= damage;
        SetHealthBar();
        Limit();

    }


    void Limit()
    {
        if (currHealth <= 0)
        {
            currHealth = 0;

            animator.SetBool("DeathTrigger", true);
            loseText.text = "You Lost!";
            //audio.PlayOneShot(SoundToPlay, Volume);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
        }
    }
}